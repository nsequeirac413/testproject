﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace MvcMovie.Models
{
    public class Cine
    {
        public int ID { get; set; }
        public string Title { get; set; }

        [Display(Name = "Open Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }
        public string Genre { get; set; }
        public decimal Price { get; set; }
    }

    public class CineDBContext : DbContext
    {
        public DbSet<Cine> Cine { get; set; }
    }
}
